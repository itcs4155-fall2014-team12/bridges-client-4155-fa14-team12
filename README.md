bridges
=======

TUES Bridges Project - uniting classrooms for better student engagement

## Getting Started



### As a Student

See http://bridgesuncc.github.io/ for tutorial. 

- Download the example project (creating a graph of twitter followers)
- Contains an existing Eclipse project, with maven configuration
  - You should use the eclipse-m2e plugin or similar to keep dependencies up to date

> If you decide to develop using your own environment, add the following dependency:
> group: "edu.uncc.cs.bridges", artifact: "client-scala", version: "0.2"

### As a Developer

- Clone this repository: `git clone https://bitbucket.org/itcs4155-fall2014-team12/bridges-client-4155-fa14-team12 bridges`
- Install [Gradle](http://gradle.org)
- Add gradle binaries to your PATH
- on Linux, you can do all of that like this: (execute these one at a time so you can react if something goes wrong)
```sh
git clone https://bitbucket.org/itcs4155-fall2014-team12/bridges-client-4155-fa14-team12 bridges
cd bridges
wget http://services.gradle.org/distributions/gradle-1.12-bin.zip
unzip gradle-1.12-all.zip
rm gradle-1.12-all.zip
# for mac osx use ~/.bash_profile in place of ~/.bashrc
echo "export PATH=\$PATH:$PWD/gradle-1.12/bin" >>~/.bashrc
# On fedora, uncomment this line too
# otherwise you will get an error telling you how javac can't be found
# sudo yum install java-1.7.0-openjdk-devel
gradle assemble
gradle --info check
```

**To build your client code, do the following:**

- Increment the version number in build.gradle (if you're planning to push at least)
- Run the following in the bridges folder:
```
gradle clean build assemble upload cleanEclipse eclipse
```

**To tell Eclipse to use your local client code:**

- If you don't already have a working BRIDGES project, follow the Hello World tutorial to create one (which includes setting up Maven and pointing it to the official BRIDGES client repository)
- Make sure the folder your client code is cloned into is called simply "bridges"
- In your pom.xml in Eclipse, find the Google Drive URL and replace it with a file url pointing to your releases folder (on Linux it should look something like what's below), then save it (Ctrl+S)
```
file:///home/username/bridges/releases/
```
- Right click on your project name in the Eclipse sidebar, and choose Maven > Update Project...
- Check the box marked "Force Update of Snapshots/Releases" and click OK
- Re-run your project, and it should be using the latest build of your code